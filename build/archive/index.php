<?php
    $con = mysqli_connect("localhost","musdash","qcfVFAOpYgOkcIJv","musdash");
    if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>BBQ Digital | Musdash</title>
    <link rel="stylesheet" href="/static/styles/css/core.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/static/styles/css/jquery.jdigiclock.css" />
    <script src="/static/scripts/lib/jquery.min.js"></script>
    <script src="/static/scripts/lib/modernizr.min.js"></script>
    <script type="text/javascript" src="/static/scripts/lib/jquery.jdigiclock.js"></script>
    <script src="/static/scripts/core.js"></script>
</head>
<body>
    <div class="grid">
        <section class="module servercheck">
        <?php
            function urlExists($url=NULL)
            {
                if($url == NULL) return false;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $data = curl_exec($ch);
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                if($httpcode>=200 && $httpcode<300){
                    return true;
                } else {
                    return false;
                }
            }
            //Array of websites to check
            $website = array('bbqdigital.com', 'withbbq.com', 'bbqpreview.com', 'digitalfusionmag.com', 'clearlymobile.withbbq.com', 'oursymphony.org', 'biggerize.it');
            echo '<ul>';
            foreach ($website as $key => $value) {
                $status = (urlExists('http://'.$value)) ? "active" : "inactive";
                echo '<li class="site-'.$status.'">'.$value.'</li>';
            }
            echo '</ul>';
        ?>

    </section>

    <section class="module jDigiClock">
        <div id="digiclock"></div>
    </section>

    <section class="module dnd">
        <?php
        if (isset($_POST['submit'])) {
           $user = $_POST['user'];
           $newStatus = $_POST['status'];
           $sql = "UPDATE moduleDND SET status = '$newStatus' WHERE user= '$user'";
           $postResult = mysqli_query($con , $sql) or die ('Error updating status');
        }
        if ($_GET['status']) {
            echo '
            <form action="index.php" method="POST">
                <fieldset>
                    <legend>Update status</legend>
                    <label for="user">I am</label>
                    <select name="user" id="user">
                        <option value="Alex">Alex</option>
                        <option value="Col">Col</option>
                    </select>
                    <label for="status">and I am</label>
                    <select name="status" id="status">
                        <option value="free">free to talk</option>
                        <option value="busy">too busy to talk</option>
                        <option value="ooo">out of the office</option>
                    </select>
                    <input type="submit" class="cancel" value="Forget it">
                    <input type="submit" name="submit" class="submit" value="Update">
                </fieldset>
            </form>
            ';
        }

        ?>
        <ul>
            <?php
            $result = mysqli_query($con,"SELECT * FROM moduleDND");

            while($value = mysqli_fetch_array($result)) {
               switch ($value['status']) {
                    case 'busy' :
                    $message = 'Do not disturb';
                    break;
                    default:
                    case 'free' :
                    $message = 'Available';
                    break;
                    case 'ooo' :
                    $message = 'Out of office';
                    break;
                }
               echo '
                    <li class="'.$value['status'].'" id="'.strtolower($value['user']).'">
                        <img src="/static/images/user-'.strtolower($value['user']).'.png" alt="'.$value['user'].'">
                        <h1>'.$value['user'].'</h1>
                        <span>'.$message.'</span>
                    </li>';
            }

            mysqli_close($con);

            ?>
        </ul>
    </section>
</div>

</body>
</html>
