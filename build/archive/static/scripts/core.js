/* global Modernizr:true */

// Namespacing
var Core = Core || {};

Core = {
    constructor: function () {
        // Detect browser
        this.isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        this.isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

        // set global variables.
        this.bodyTag = $('body');
        this.bodyTag.removeClass('no-js').addClass('js'); // We know js has been detected so add the 'js' class to the page.
        this.viewportHeight = this.bodyTag.outerHeight(true);
        this.viewportWidth = this.bodyTag.outerWidth(true);
    },

    init: function () {
        var o = this;
        o.constructor();
        o.keepUpdated();
        o.jdigiclockInit();
        //o.responsiveLogger(); // Only turn on in dev environment
    },

    keepUpdated: function () {
        var o = this;
        setInterval(
            function(){
                o.ajaxCheck();
            }, 1000);
    },

    ajaxCheck: function () {
        var o = this,
            message = null,
            color = null,
            dnd = o.bodyTag.find('.dnd');
        $.getJSON( "quickstatus.php", function( data ) {
          $.each( data.users, function( i, item ) {
            switch (item.status) {
                case 'busy' :
                    message = 'Do not disturb';
                    color = '#d12312';
                    break;
                    default:
                    case 'free' :
                    message = 'Available';
                    color = '#007E00';
                    break;
                    case 'ooo' :
                    message = 'Out of office';
                    color = '#555';
                    break;
            }
            var user = item.user,
                status = item.status;
            var userBox = dnd.find('#'+user.toLowerCase()).removeClass().addClass(status);
            userBox.find('span').text(message)
          });
        });
    },

    responsiveLogger: function() {
        // Output the screen width (For development only this method should be removed when the site is deployed)
        var o = this;
        o.screenLogger = $('<div style="position:absolute;left:5px;top:5px;padding:10px;font-size:12px;background:black;color:#fff;z-index:10000;opacity:0.8"></div>');
        o.screenLogger.appendTo('body');
        setInterval(
            function() {
                o.viewportWidth = $('body').outerWidth(true);
                o.screenLogger.html(o.viewportWidth+'px');
            }, 500
        );
    },

    jdigiclockInit: function () {
        $('#digiclock').jdigiclock({
               clockImagesPath: '/static/images/clock/clock/',
               weatherImagesPath: '/static/images/clock/weather/',
               lang: 'en',
               am_pm: false,
               weatherLocationCode: 'EUR|UK|UK001|SOUTHAMPTON',
               weatherMetric: 'C',
               weatherUpdate: 0,
               proxyType: 'php'
        });
        $('#digiclock').find('#left_arrow, #right_arrow').remove();
    }
};

$(document).ready( function() {
    Core.init();
});

