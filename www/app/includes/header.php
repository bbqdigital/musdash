<?php
    $con = mysqli_connect("localhost","6JvYhRR2YBbGs","wnoDItXVWHtsQyV7N","musdash");
    if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Musdash</title>
    <link rel="stylesheet" href="/static/styles/css/core.css" />
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <script src="/static/scripts/lib/jquery.min.js"></script>
    <script src="/static/scripts/lib/modernizr.min.js"></script>
    <link rel="shortcut icon" href="/static/images/logos/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/static/images/logos/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/static/images/logos/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/static/images/logos/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/static/images/logos/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/static/images/logos/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/static/images/logos/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/static/images/logos/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/static/images/logos/apple-touch-icon-152x152.png" />
</head>
<body>
    <header>
    <img src="/static/images/musdash-logo-smaller.png" alt="Musdash Logo">
    </header>
    <div class="grid">
