{
    "users" : [
<?php

    function humanTiming ($time) {
        $time = time() - $time; // to get the time since that moment
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }
    }

    $con = mysqli_connect("localhost","6JvYhRR2YBbGs","wnoDItXVWHtsQyV7N","musdash");
    if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
    $result = mysqli_query($con,"SELECT * FROM moduleDND");
    $count = mysqli_num_rows($result);
    $i = 0;
        while($value = mysqli_fetch_array($result)) {
            switch ($value['status']) {
                case 'busy' :
                    $message = 'Fairly busy';
                break;
                default:
                case 'dnd' :
                    $message = 'Do not disturb';
                break;
                case 'free' :
                    $message = 'Available';
                break;
                case 'ooo' :
                    $message = 'Out of office';
                break;
                case 'done' :
                    $message = 'Done for the day';
                break;
            }
            ?>
                { "user": "u-<?php echo $value['id']; ?>", "status": "<?php echo $value['status']; ?>", "timestamp": "<?php echo humanTiming($value['last_updated']); ?>" }
            <?php
            if ($i < $count-1) echo ',';
            $i++;
        }
        mysqli_close($con);
?>
    ]
}
