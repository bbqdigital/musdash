<?php require_once('../../includes/header.php'); ?>
    <section class="module dnd quickstatus">
        <ul>
            <?php
            $result = mysqli_query($con,"SELECT * FROM moduleDND");

            while($value = mysqli_fetch_array($result)) {
               switch ($value['status']) {
                    case 'busy' :
                    $message = 'Do not disturb';
                    break;
                    default:
                    case 'free' :
                    $message = 'Available';
                    break;
                    case 'ooo' :
                    $message = 'Out of office';
                    break;
                }
               echo '
                    <li class="'.$value['status'].'" id="'.strtolower($value['user']).'">
                        <div class="cell"><h1>'.$value['user'].'</h1></div>
                        <div class="cell status"><span>'.$message.'</span></div>
                    </li>';
            }

            mysqli_close($con);

            ?>
        </ul>
    </section>
<?php require_once('../../includes/footer.php'); ?>
