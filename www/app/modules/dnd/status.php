<?php require_once('../../includes/header.php'); ?>
    <section class="module dnd">
        <?php
        if (isset($_POST['submit'])) {
           $user = $_POST['user'];
           $newStatus = $_POST['status'];
           $timestamp = $_SERVER['REQUEST_TIME'];
           $sql = "UPDATE moduleDND SET status = '$newStatus', last_updated = '$timestamp' WHERE user= '$user'";
           $postResult = mysqli_query($con , $sql) or die ('Error updating status');
        }
        if (isset($_GET['status'])) {
            echo '
            <form action="status.php" method="POST">
                <fieldset>
                    <legend>Update status</legend>
                    <input type="hidden" name="user" id="user" value="'.$_GET['user'].'">
                    <label class="hide" for="status">I am</label>
                    <select name="status" id="status">
                        <option value="free">I am free to talk</option>
                        <option value="busy">I am quite busy</option>
                        <option value="dnd">I cannot be disturbed</option>
                        <option value="ooo">I am out of the office</option>
                        <option value="done">I am done for the day</option>
                    </select>
                    <input type="submit" class="cancel" value="Forget it">
                    <input type="submit" name="submit" class="submit" value="Update">
                </fieldset>
            </form>
            ';
        }

        ?>
        <ul>
            <?php

                function humanTiming ($time) {
                    $time = time() - $time; // to get the time since that moment
                    $tokens = array (
                        31536000 => 'year',
                        2592000 => 'month',
                        604800 => 'week',
                        86400 => 'day',
                        3600 => 'hour',
                        60 => 'minute',
                        1 => 'second'
                    );

                    foreach ($tokens as $unit => $text) {
                        if ($time < $unit) continue;
                        $numberOfUnits = floor($time / $unit);
                        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
                    }
                }

            $result = mysqli_query($con,"SELECT * FROM moduleDND");

            while($value = mysqli_fetch_array($result)) {
               switch ($value['status']) {
                    case 'busy' :
                    $message = 'Fairly busy';
                    break;
                    default:
                    case 'dnd' :
                    $message = 'Do not disturb';
                    break;
                    case 'free' :
                    $message = 'Available';
                    break;
                    case 'ooo' :
                    $message = 'Out of office';
                    break;
                    case 'done' :
                    $message = 'Done for the day';
                    break;
                }
               echo '
                    <li class="'.$value['status'].'" id="'.strtolower($value['user']).'">
                        <img src="/app/static/images/user-'.strtolower($value['user']).'.png" alt="'.$value['user'].'">
                        <h1>'.$value['user'].'</h1>
                        <span class="message">'.$message.'</span>
                        <span class="timestamp">'.humanTiming($value['last_updated']).'</span>
                    </li>';
            }

            mysqli_close($con);

            ?>
        </ul>
    </section>
<?php require_once('../../includes/footer.php'); ?>
