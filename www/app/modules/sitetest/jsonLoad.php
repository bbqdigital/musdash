 {
    "sites" : [
        <?php
        function Visit($url){
               $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";$ch=curl_init();
               curl_setopt ($ch, CURLOPT_URL,$url );
               curl_setopt($ch, CURLOPT_USERAGENT, $agent);
               curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
               curl_setopt ($ch,CURLOPT_VERBOSE,false);
               curl_setopt($ch, CURLOPT_TIMEOUT, 5);
               curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
               curl_setopt($ch,CURLOPT_SSLVERSION,3);
               curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, FALSE);
               $page=curl_exec($ch);
               //echo curl_error($ch);
               $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
               curl_close($ch);
               if($httpcode>=200 && $httpcode<400) return true;
               else return false;
        }

        function slugify($text) {
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
            $text = trim($text, '-');
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = strtolower($text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            if (empty($text)) { return 'n-a'; }
            return $text;
        }

        $con = mysqli_connect("localhost","6JvYhRR2YBbGs","wnoDItXVWHtsQyV7N","musdash");
        if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
        $result = mysqli_query($con,"SELECT * FROM moduleSiteTest");
        $count = mysqli_num_rows($result);
        $i = 0;
        while($value = mysqli_fetch_array($result)) {
            $url = $value['url'];
            $name = $value['name'];
            $icon = $value['icon_path'];
            $status = (Visit($url)) ? "green" : "red";
            $icon = ($icon) ? '//'.$url.$icon : 'static/images/star.png';
        ?>
            { "site": "<?php echo slugify($name); ?>", "status": "<?php echo $status; ?>" }
            <?php
            if ($i < $count-1) echo ',';
            $i++;
        }
        ?>
    ]
}
