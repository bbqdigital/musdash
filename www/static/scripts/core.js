/* global Modernizr:true */

// Namespacing
var Core = Core || {};

function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

Core = {
    constructor: function () {

        // set global variables.
        this.bodyTag = $('body');
        this.bodyTag.removeClass('no-js').addClass('js'); // We know js has been detected so add the 'js' class to the page.
        this.viewportHeight = this.bodyTag.outerHeight(true);
        this.viewportWidth = this.bodyTag.outerWidth(true);
    },

    init: function () {
        var o = this;
        o.constructor();
        o.keepUpdated();
        o.updateStatus();
        o.clockInit();
        //o.responsiveLogger(); // Only turn on in dev environment
    },

    // Fires every second to check if someone has updated their status
    keepUpdated: function () {
        var o = this;
        setInterval(
            function(){
                o.dndCheck();
        }, 1000);
        setInterval( function() {
            o.siteCheck();
        }, 180000);
    },

    // If a user clicks on their name, they can update their status
    updateStatus: function () {
        var o = this;
        o.bodyTag.find('.dnd li').click( function() {
            var thisUser = $(this).attr('id').replace('u-', '');
            window.location = '?status=true&user='+thisUser;
        });
    },

    siteCheck: function() {
        var o = this,
            message = null,
            color = null,
            st = o.bodyTag.find('.sitetest');
        $.getJSON( "/app/modules/sitetest/jsonLoad.php", function( data ) {
            $.each( data.sites, function( i, item ) {
                var site = item.site,
                    sitetestBox = st.find('#'+site);
                    if (sitetestBox.attr('class') !== item.status) {
                        sitetestBox.removeClass().addClass(item.status);
                    }
                    if (item.status === 'red') {
                        // A site has failed. Show an alert but only once.
                        if (readCookie(item.site+'-alert') == null) {
                            var alert = $('<div class="site-alert"><span>'+item.site+' is not responding<span><span class="timer"></span></div>').prependTo(o.bodyTag);
                            var countdown = 4,
                                counter = 0;
                            setInterval(function() {
                                if (counter === countdown) alert.remove();
                                alert.find('.timer').text('This message will dismiss in '+(countdown-counter)+' seconds');
                                counter++;
                            }, 1000);
                            createCookie(item.site+'-alert', true);
                        }
                    } else {
                        // Site status returned to normal. Erase alert cookie
                        eraseCookie(item.site+'-alert');
                    }
            });
      });
    },

    dndCheck: function () {
        var o = this,
            message = null,
            color = null,
            dnd = o.bodyTag.find('.dnd');
        $.getJSON( "/app/modules/dnd/jsonLoad.php", function( data ) {
          $.each( data.users, function( i, item ) {
            switch (item.status) {
                case 'dnd' :
                    message = 'Do not disturb';
                break;
                case 'busy' :
                    message = 'Fairly busy';
                break;
                case 'free' :
                    message = 'Available';
                break;
                case 'ooo' :
                    message = 'Out of office';
                break;
                case 'done' :
                    message = 'Done for the day';
                break;
            }
            var user = item.user,
                status = item.status;
            var userBox = dnd.find('#'+user.toLowerCase());
            if (userBox.attr('class') !== item.status) {
                userBox.removeClass().addClass('flash').addClass(status).find('.message').text(message)
                setTimeout(function() { userBox.removeClass('flash'); }, 300);
            }
            userBox.find('.timestamp').text(item.timestamp);
          });
        });
    },

    responsiveLogger: function() {
        // Output the screen width (For development only this method should be removed when the site is deployed)
        var o = this;
        o.screenLogger = $('<div style="position:absolute;left:5px;top:5px;padding:10px;font-size:12px;background:black;color:#fff;z-index:10000;opacity:0.8"></div>');
        o.screenLogger.appendTo('body');
        setInterval(
            function() {
                o.viewportWidth = $('body').outerWidth(true);
                o.screenLogger.html(o.viewportWidth+'px');
            }, 500
        );
    },

    clockInit: function () {
        var o = this,
            clock = o.bodyTag.find('.clock').html(''),
            hourContainer = $('<span class="hour">00</span>').appendTo(clock),
            blink = $('<span class="blink">:</span>').appendTo(clock),
            minuteContainer = $('<span class="minute">00</span>').appendTo(clock);

        //make clock
        var makeClock = function () {
            var dt = new Date(),
                currentHour = ("0" + dt.getHours()).slice(-2),
                currentMinute = ("0" + dt.getMinutes()).slice(-2);
            hourContainer.removeClass(function (index, css) {
                return (css.match (/(^|\s)ts-\S+/g) || []).join(' ');
            }).addClass('ts-'+currentHour).html(currentHour);
            minuteContainer.removeClass(function (index, css) {
                return (css.match (/(^|\s)ts-\S+/g) || []).join(' ');
            }).addClass('ts-'+currentMinute).html(currentMinute);
        }

        makeClock(); //initial run
        setInterval(function() { makeClock(); }, 30000);
    }
};

$(document).ready( function() {
    Core.init();
});

